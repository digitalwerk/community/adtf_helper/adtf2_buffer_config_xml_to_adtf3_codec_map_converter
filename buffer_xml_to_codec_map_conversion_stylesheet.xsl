<?xml version="1.0" encoding="iso-8859-1" standalone="no"?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="yes"/>
    <!-- settings -->
    <xsl:template match="settings">
        <mapping>
            <xsl:choose>
                <xsl:when test="@codec">
                    <xsl:attribute name="codec">
                        <xsl:value-of select="@codec"/>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <xsl:choose>
                <xsl:when test="@version">
                    <xsl:attribute name="version">
                        <xsl:value-of select="@version"/>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <xsl:if test="./input/buffer and ./output/buffer">
                <!-- WARNING: input file can have both input and output nodes although no longer supported -->
                <xsl:message>WARNING: input file can have both input and output nodes although no longer supported</xsl:message>
            </xsl:if>
            <xsl:apply-templates select="input"/>
            <xsl:apply-templates select="output"/>
        </mapping>
    </xsl:template>
    <!-- input -->
    <xsl:template match="input">
        <outputs>
            <xsl:choose>
                <xsl:when test="@channel">
                    <xsl:attribute name="channel">
                        <xsl:choose>
                            <xsl:when test="@channel=0">
                                <xsl:value-of select="'1'"/>
                                <xsl:message>WARNING: channel 0 mapped to channel 1</xsl:message>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="@channel"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <xsl:apply-templates select="./buffer"/>
        </outputs>
    </xsl:template>
    <!-- output -->
    <xsl:template match="output">
        <inputs>
            <xsl:choose>
                <xsl:when test="@channel">
                    <xsl:attribute name="channel">
                        <xsl:choose>
                            <xsl:when test="@channel=0">
                                <xsl:value-of select="'1'"/>
                                <xsl:message>WARNING: channel 0 mapped to channel 1</xsl:message>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="@channel"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
            <xsl:apply-templates select="./buffer"/>
        </inputs>
    </xsl:template>
    <!-- buffer -->
    <xsl:template name="buffer_attribute_set">
        <!-- list of required attributes-->
        <xsl:attribute name="name">
            <xsl:value-of select="@name"/>
        </xsl:attribute>
        <!-- list of optional attributes-->
        <xsl:if test="@delay">
            <xsl:attribute name="delay">
                <xsl:value-of select="@delay"/>
            </xsl:attribute>
        </xsl:if>
        <xsl:if test="@daq_event">
            <xsl:attribute name="daq_event">
                <xsl:value-of select="@daq_event"/>
            </xsl:attribute>
        </xsl:if>
        <xsl:if test="@polling_interval">
            <xsl:attribute name="polling_interval">
                <xsl:value-of select="@polling_interval"/>
            </xsl:attribute>
        </xsl:if>
        <xsl:if test="@daq_prescaler">
            <xsl:attribute name="daq_prescaler">
                <xsl:value-of select="@daq_prescaler"/>
            </xsl:attribute>
        </xsl:if>
        <xsl:if test="@clear_after_transmit">
            <xsl:attribute name="clear_after_transmit">
                <xsl:value-of select="@clear_after_transmit"/>
            </xsl:attribute>
        </xsl:if>
        <xsl:if test="@timestamp">
            <xsl:attribute name="timestamp">
                <xsl:value-of select="@timestamp"/>
            </xsl:attribute>
        </xsl:if>
        <xsl:apply-templates select="struct"/>
    </xsl:template>
    <!-- settings/input/buffer -->
    <xsl:template match="settings/input/buffer">
        <!--This is the output pin's name -->
        <output>
            <!-- WARNING assuming buffers have same attribute sets -->
            <xsl:call-template name="buffer_attribute_set"/>
        </output>
    </xsl:template>
    <!-- settings/output/buffer -->
    <xsl:template match="settings/output/buffer">
        <!-- This is the input pin's name -->
        <input>
            <!-- WARNING assuming buffers have same attribute sets -->
            <xsl:call-template name="buffer_attribute_set"/>
        </input>
    </xsl:template>
    <!-- struct -->
    <xsl:template match="struct">
    <struct>
        <xsl:choose>
            <xsl:when test="@name">
                <xsl:attribute name="name">
                    <xsl:value-of select="@name"/>
                </xsl:attribute>
            </xsl:when>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="@array">
                <xsl:attribute name="array">
                    <xsl:value-of select="@array"/>
                </xsl:attribute>
            </xsl:when>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="@pack">
                <xsl:attribute name="pack">
                    <xsl:value-of select="@pack"/>
                </xsl:attribute>
            </xsl:when>
        </xsl:choose>
        <xsl:apply-templates select="structname"/>
        <xsl:apply-templates select="element"/>
        <xsl:for-each select="struct">
            <struct>
                <xsl:choose>
                    <xsl:when test="@name">
                        <xsl:attribute name="name">
                            <xsl:value-of select="@name"/>
                        </xsl:attribute>
                    </xsl:when>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="@array">
                        <xsl:attribute name="array">
                            <xsl:value-of select="@array"/>
                        </xsl:attribute>
                    </xsl:when>
                </xsl:choose>
                <xsl:choose>
                    <xsl:when test="@pack">
                        <xsl:attribute name="pack">
                            <xsl:value-of select="@pack"/>
                        </xsl:attribute>
                    </xsl:when>
                </xsl:choose>
                <xsl:apply-templates select="element"/>
                <xsl:for-each select="struct">
                    <struct>
                        <xsl:choose>
                            <xsl:when test="@name">
                                <xsl:attribute name="name">
                                    <xsl:value-of select="@name"/>
                                </xsl:attribute>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:choose>
                            <xsl:when test="@array">
                                <xsl:attribute name="array">
                                    <xsl:value-of select="@array"/>
                                </xsl:attribute>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:choose>
                            <xsl:when test="@pack">
                                <xsl:attribute name="pack">
                                    <xsl:value-of select="@pack"/>
                                </xsl:attribute>
                            </xsl:when>
                        </xsl:choose>
                        <xsl:apply-templates select="element"/>
                    </struct>
                </xsl:for-each>
            </struct>
        </xsl:for-each>
    </struct>
    </xsl:template>

    <!-- structname -->


    <!-- element -->
    <xsl:template match="element">
        <!-- check 'invalid file' assignment -->
        <xsl:choose>
            <xsl:when test="@invalid_signal">
                <xsl:comment> Invalid Signal: <xsl:value-of select="@name"/> 
                </xsl:comment>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="@deleted_signal">
                        <xsl:comment> Deleted Signal: <xsl:value-of select="@name"/> 
                        </xsl:comment>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:choose>
                            <xsl:when test="@invalid_ADTF_signal">
                                <xsl:comment> Invalid ADTF Signal: <xsl:value-of select="@name"/> 
                                </xsl:comment>
                            </xsl:when>
                            <xsl:otherwise>
                                <assignment>    
                                    <!-- list of optional attributes-->
                                    <xsl:if test="@timestamp">
                                        <xsl:attribute name="bits">
                                            <xsl:value-of select="@bits"/>
                                        </xsl:attribute>
                                    </xsl:if>
                                    <!-- list of required tags -->
                                    <to>
                                        <xsl:value-of select="@name"/>
                                    </to>
                                    <from>
                                        <xsl:value-of select="@signal"/>
                                    </from>
                                    <type>
                                        <xsl:value-of select="@type"/>
                                    </type>
                                    <!-- list of optional tags -->
                                    <xsl:if test="@value">
                                        <value>
                                            <xsl:value-of select="@value"/>
                                        </value>
                                    </xsl:if>
                                    <xsl:if test="@default_value">
                                        <default_value>
                                            <xsl:value-of select="@default_value"/>
                                        </default_value>
                                    </xsl:if>
                                    <xsl:if test="@unit_conversion">
                                        <unit_conversion>
                                            <xsl:value-of select="@unit_conversion"/>
                                        </unit_conversion>
                                    </xsl:if>
                                    <xsl:if test="@limits">
                                        <limits>
                                            <xsl:value-of select="@limits"/>
                                        </limits>
                                    </xsl:if>
                                    <xsl:if test="@sign_signal">
                                        <sign_signal>
                                            <xsl:value-of select="@sign_signal"/>
                                        </sign_signal>
                                    </xsl:if>
                                    <xsl:if test="@count">
                                        <count>
                                            <xsl:value-of select="@count"/>
                                        </count>
                                    </xsl:if>
                                    <xsl:if test="@corrective_factor">
                                        <corrective_factor>
                                            <xsl:value-of select="@corrective_factor"/>
                                        </corrective_factor>
                                    </xsl:if>
                                    <xsl:if test="@corrective_offset">
                                        <corrective_offset>
                                            <xsl:value-of select="@corrective_offset"/>
                                        </corrective_offset>
                                    </xsl:if>
                                    <xsl:if test="@value_type">
                                        <value_type>
                                            <xsl:value-of select="@value_type"/>
                                        </value_type>
                                    </xsl:if>
                                </assignment>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
