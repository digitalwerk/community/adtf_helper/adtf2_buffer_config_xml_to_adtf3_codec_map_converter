# Documentation - ADTF2 BUFFER CONFIG XML to ADTF3 CODEC MAP FILE CONVERTER

In ADTF2 and ADTF3 the Device and Calibration Toolbox provide support for bus protocols, database and communication. 
For each purpose there are filter(s) for encoding and decoding to map DDL described data structures to signals of related database and vise versa.

In ADTF2 you have to define a buffer config xml file for creating pins and structures inside a Bus Encoder/Decoder. 
In ADTF3 this behaviour is almost comparable but the file format differs - a so called codec map file. 

If you set up a new ADTF3 session from scratch you implicit use the new format and everything will be fine. 

But if you try to migarte your use case from a valid ADTF2 configuration to an ADTF3 session, you can not 
reuse an existing buffer configuration xml file from ADTF2 within the related ADTF3 filter.

There are good reasons why the format has been changed and the former buffer configuration xml file 
can not be (legacy) supported, but to assist you to not rewrite the new codec map file by yourself, 
here is a small XSL stylesheet to convert your setup.

Feel free to use it with any XSLT compatible executable you want.

If you are missing features, feel free to extend it and create a merge request to share with the community.